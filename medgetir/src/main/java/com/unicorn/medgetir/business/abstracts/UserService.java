package com.unicorn.medgetir.business.abstracts;

import java.util.List;

import org.springframework.stereotype.Service;

import com.unicorn.medgetir.dto.UserDTO;
import com.unicorn.medgetir.model.User;

@Service
public interface UserService {
	
    User add(UserDTO userDTO) ;
   
    List<User> findByName(String name);
    
	User findByPhone(String phone);
	
	User updateUser(UserDTO userDTO);
	
	User getById(int id);
	
	User findByPhoneAndPassword(String phone,String password);
		
	

}