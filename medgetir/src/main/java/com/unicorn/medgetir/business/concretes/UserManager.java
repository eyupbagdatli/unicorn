package com.unicorn.medgetir.business.concretes;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unicorn.medgetir.business.abstracts.UserService;
import com.unicorn.medgetir.dto.UserDTO;
import com.unicorn.medgetir.model.User;
import com.unicorn.medgetir.repository.UserRepository;

@Service
public class UserManager implements UserService {

	
	@Autowired
	UserRepository userRepository;
	
	
	@Override
	public User add(UserDTO userDTO) {

		User user =new User();
		BeanUtils.copyProperties(userDTO, user);
		User newUser=userRepository.save(user);
		return newUser;
	}


	@Override
	public List<User> findByName(String name) {
	return userRepository.findByName(name);
	}


	@Override
	public User findByPhone(String phone) {
		
		return userRepository.findByPhone(phone);
	}


	@Override
	public User updateUser(UserDTO userDTO) {
		
		User newUser=userRepository.getById(userDTO.getId());
		newUser.setName(userDTO.getName());
		newUser.setSurname(userDTO.getSurname());
		newUser.setPassword(userDTO.getPassword());
		newUser.setNationality_Id(userDTO.getNationality_Id());
		newUser.setDateOfBirth(userDTO.getDateOfBirth());
		newUser.setPhone(userDTO.getPhone());
		return userRepository.save(newUser);
		
		
	}


	@Override
	public User getById(int id) {
		
		return userRepository.getById(id);
	}


	@Override
	public User findByPhoneAndPassword(String phone, String password) {
		
		return userRepository.findByPhoneAndPassword(phone, password);
	}


	

}
