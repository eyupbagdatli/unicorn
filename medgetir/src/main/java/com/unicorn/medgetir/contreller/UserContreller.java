package com.unicorn.medgetir.contreller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unicorn.medgetir.business.abstracts.UserService;
import com.unicorn.medgetir.dto.UserDTO;
import com.unicorn.medgetir.model.User;



@RestController
@RequestMapping("/user")
public class UserContreller {
	
	
	@Autowired
	UserService userService;

	@PostMapping("/add-user")
	public User add(@Valid@RequestBody UserDTO userDTO) {
		return userService.add(userDTO);
		}
	
	@GetMapping("user-name/{name}")
	public List<User> getAll(@PathVariable("name") String name) {
		return userService.findByName(name);
	}
	
	@GetMapping("getById")
	public User getById(@RequestParam int id) {
		return userService.getById(id);
	} 
	
	@GetMapping("findByPhone")
	public User findByPhone(@RequestParam String phone) {
		return userService.findByPhone(phone);
	}
	
	@GetMapping("findByPhoneAndPassword")
	public User findByPhoneAndPassword(String phone, String password) {
		return userService.findByPhoneAndPassword(phone, password);
	}
	
	
	@PutMapping("/update")
	public User updateUser(@RequestBody UserDTO userDTO) {
		return userService.updateUser(userDTO);
	}
	
	
	
}
