package com.unicorn.medgetir;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedgetirApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedgetirApplication.class, args);
	}

}
