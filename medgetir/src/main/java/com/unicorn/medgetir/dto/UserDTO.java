package com.unicorn.medgetir.dto;

import lombok.Data;

@Data
public class UserDTO {
	
	private int id;
	private String name;
	private String surname;
	private String password;
	private String nationality_Id;
	private String phone;
	private String dateOfBirth;

}
